#input only integer
while True:
	try:
		year = int(input('Please input year\n'))
		break
	except ValueError:
		print("input integers only please!")
		continue

if (year%4==0 and year%100!=0) or (year%400==0):
	print(year, "is a leap year")
else :
	print(year, "is not a leap year")

star = "*"
row = int(input('Please input rows\n'))
col = int(input('Please input columns\n'))

for x in range(row):
	print(star*col+'')